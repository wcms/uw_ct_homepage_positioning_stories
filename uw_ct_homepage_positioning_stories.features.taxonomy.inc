<?php
/**
 * @file
 * uw_ct_homepage_positioning_stories.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_homepage_positioning_stories_taxonomy_default_vocabularies() {
  return array(
    'homepage_positioning_areas' => array(
      'name' => 'Positioning Areas',
      'machine_name' => 'homepage_positioning_areas',
      'description' => 'For Positioning Stories, the 4 positioning areas.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
