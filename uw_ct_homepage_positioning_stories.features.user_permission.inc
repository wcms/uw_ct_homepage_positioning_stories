<?php
/**
 * @file
 * uw_ct_homepage_positioning_stories.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_positioning_stories_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_positioning_stories content'.
  $permissions['create homepage_positioning_stories content'] = array(
    'name' => 'create homepage_positioning_stories content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_positioning_stories content'.
  $permissions['delete any homepage_positioning_stories content'] = array(
    'name' => 'delete any homepage_positioning_stories content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_positioning_stories content'.
  $permissions['delete own homepage_positioning_stories content'] = array(
    'name' => 'delete own homepage_positioning_stories content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any homepage_positioning_stories content'.
  $permissions['edit any homepage_positioning_stories content'] = array(
    'name' => 'edit any homepage_positioning_stories content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_positioning_stories content'.
  $permissions['edit own homepage_positioning_stories content'] = array(
    'name' => 'edit own homepage_positioning_stories content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter homepage_positioning_stories revision log entry'.
  $permissions['enter homepage_positioning_stories revision log entry'] = array(
    'name' => 'enter homepage_positioning_stories revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_positioning_stories authored by option'.
  $permissions['override homepage_positioning_stories authored by option'] = array(
    'name' => 'override homepage_positioning_stories authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_positioning_stories authored on option'.
  $permissions['override homepage_positioning_stories authored on option'] = array(
    'name' => 'override homepage_positioning_stories authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_positioning_stories promote to front page option'.
  $permissions['override homepage_positioning_stories promote to front page option'] = array(
    'name' => 'override homepage_positioning_stories promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_positioning_stories published option'.
  $permissions['override homepage_positioning_stories published option'] = array(
    'name' => 'override homepage_positioning_stories published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_positioning_stories revision option'.
  $permissions['override homepage_positioning_stories revision option'] = array(
    'name' => 'override homepage_positioning_stories revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_positioning_stories sticky option'.
  $permissions['override homepage_positioning_stories sticky option'] = array(
    'name' => 'override homepage_positioning_stories sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
