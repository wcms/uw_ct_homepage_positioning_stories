<?php
/**
 * @file
 * uw_ct_homepage_positioning_stories.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_positioning_stories_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_homepage_positioning_stories_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_positioning_stories_node_info() {
  $items = array(
    'homepage_positioning_stories' => array(
      'name' => t('Positioning Stories (no longer used)'),
      'base' => 'node_content',
      'description' => t('Highlights Waterloo via stories that demonstrate the positioning areas of innovations, connections, impact and global view. This content type is no longer in use.'),
      'has_title' => '1',
      'title_label' => t('Label'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
